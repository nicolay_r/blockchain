Get account balance of different blockchain currencies
 
1. Создать levels.txt где для монеты указана цена в usd, например:
```
btc 10000
eth 700
```

2. Создать ids.txt где для указаны пары монета -- id кошелька, разделенные пробелом
```
btc <btd_wallet_id>
eth <eth_wallet_id>
...
```