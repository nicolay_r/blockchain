from balance import getBalance
from coinmarketcap import Market

class WalletInfo:

    FIAT = {'rub', 'usd'}

    def __init__(self, coin_symbol, wallet_id, symbol_to_id):
        """ Create wallet and coin information
        """
        market = Market()

        self.balance = getBalance(coin_symbol, wallet_id)
        self.coin_name = symbol_to_id[coin_symbol.upper()]

        self.wallet_id = wallet_id
        self.coin_symbol = coin_symbol
        self.balance = balance

        self.price = {}

        for fiat in FIAT:
            coin_info = market.ticker(coin_name, convert=fiat)[0]
            price = float(coin_info["price_{}".format(fiat)])
            self.price[fiat] = price

        return info

    @property
    def Price(self, fiat):
        pass

    @property
    def Balance(self, fiat):
        pass
