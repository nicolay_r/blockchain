#!/usr/bin/python
from balance import getBalance
from coinmarketcap import Market


FIAT = {'rub', 'usd'}


def get_diff_percent(actual_price, level_price):
    assert(type(actual_price) == float)
    assert(type(level_price) == float)
    return actual_price / level_price


def wallet_info(coin_symbol, wallet_id, symbol_to_id, levels_usd):
    """ Get wallet and coin information
    """

    balance = getBalance(coin_symbol, wallet_id)
    if (coin_symbol.upper() not in symbol_to_id):
        return None

    coin_name = symbol_to_id[coin_symbol.upper()]

    info = {'id': wallet_id,
            'symbol': coin_symbol,
            'balance': balance}

    for fiat in FIAT:
        coin_info = market.ticker(coin_name, convert=fiat)[0]
        price = float(coin_info["price_{}".format(fiat)])

        info['price_{}'.format(fiat)] = price
        info['balance_{}'.format(fiat)] = balance * price

    info['diff'] = get_diff_percent(info['price_usd'], levels_usd[coin_symbol])

    return info


def display_wallet_info(info):
    print "{} ({}):".format(info['id'], info['symbol'])
    print "\tbalance: {}".format(info['balance'])

    for fiat in FIAT:
        print "\t{}: {} (1 {} = {})".format(
                fiat,
                info['balance_{}'.format(fiat)],
                info['symbol'],
                info['price_{}'.format(fiat)])

    display_wallet_diff(info)


def display_wallet_diff(info):
    print "\tdiff: x{}".format("%.2f" % info['diff'])
    for fiat in FIAT:
        balance = info['balance_{}'.format(fiat)]
        initial_balance = balance / info['diff']
        diff = balance - initial_balance
        print "\t\t{}: {} ({} initially)".format(fiat, diff, initial_balance)


def display_total_info(infos):
    print "Total balance: "
    for fiat in FIAT:
        print "\t{}: {}".format(
                fiat,
                sum(map(lambda i: i['balance_{}'.format(fiat)], infos)))


market = Market()

# initialize coinmarketcap coin ids
symbol_to_id = {}
for coin in market.ticker():
    symbol_to_id[coin['symbol']] = coin['id']


# reading levels
levels = {}
with open("levels.txt", "r") as f:
    for line in f.readlines():
        record = line.split()
        symbol = record[0]
        price = float(record[1])
        levels[symbol] = price

# Reading informations
infos = []
with open("ids.txt", "r") as f:
    for line in f.readlines():
        record = line.split()
        info = wallet_info(record[0], record[1], symbol_to_id, levels)
        if info is not None:
            infos.append(info)


# display information
for info in infos:
    display_wallet_info(info)

print "-----"
display_total_info(infos)
