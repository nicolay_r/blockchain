import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning, \
    InsecurePlatformWarning, SNIMissingWarning
import json


requests.packages.urllib3.disable_warnings(SNIMissingWarning)
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)


def getBalanceChainz(coin_symbol, wallet):
    """
    returns: str
        balance Using chainz API
    """
    uri = 'https://chainz.cryptoid.info/{coin_symbol}/api.dws?q={request}&a={wallet}'.format(
        coin_symbol=coin_symbol, request='getbalance', wallet=wallet)
    balance = float(requests.get(uri).content)
    return balance


def getBalanceXRP(wallet):
    uri = 'https://data.ripple.com/v2/accounts/{}/balances'.format(wallet)
    r = requests.get(uri)
    balance = float(json.loads(r.content)['balances'][0]['value'])
    return balance


def getBalanceBTC(wallet):
    uri = 'https://blockchain.info/q/addressbalance/{}'.format(wallet)
    satoshi = requests.get(uri).content
    return int(satoshi) * 10e-9


def getBalanceETH(wallet):
    uri = 'https://api.blockcypher.com/v1/eth/main/addrs/{}'.format(wallet)
    r = requests.get(uri)
    balance = int(json.loads(r.content)['balance']) * 10e-19
    return balance

def getBalanceNLC2(wallet):
    uri = 'http://nolimitcoin.info/ext/getbalance/{}'.format(wallet)
    r = requests.get(uri)
    print r.content
    # TODO.
    return float(0)

def getBalance(coin_symbol, wallet):
    assert(type(coin_symbol) == str)
    assert(type(wallet) == str)

    balance = 0
    if (coin_symbol == 'xrp'):
        balance = getBalanceXRP(wallet)
    elif (coin_symbol == 'btc'):
        balance = getBalanceBTC(wallet)
    elif (coin_symbol == 'eth'):
        balance = getBalanceETH(wallet)
    elif (coin_symbol == 'nlc2'):
        balance = getBalanceNLC2(wallet)
    else:
        balance = getBalanceChainz(coin_symbol, wallet)

    assert(type(balance) == float)
    return balance
